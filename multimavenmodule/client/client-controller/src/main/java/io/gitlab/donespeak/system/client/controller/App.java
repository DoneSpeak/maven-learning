package io.gitlab.donespeak.system.client.controller;

import io.gitlab.donespeak.system.common.DStringUtils;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        String link = DStringUtils.concat("a", ",", "b");
        System.out.println(link);
    }
}
