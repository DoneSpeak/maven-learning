 package io.gitlab.donespeak.system.common;

 /**
 * @author Guanrong Yang
 * @date 2019/07/16
 */
public class DStringUtils {

    public static String concat(String ...parts) {
        if(parts == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for(String part: parts) {
            sb.append(part);
        }
        return sb.toString();
    }
}
