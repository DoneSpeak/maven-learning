package io.gitlab.donespeak.system.dspringboot.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Guanrong Yang
 * @date 2019/07/16
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @RequestMapping("/{userId:\\d+}")
    public Map<String, String> getUser(@PathVariable long userId) {
        System.out.println("userId: " + userId);
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "peter");
        map.put("userId", userId + "");
        
        return map;
    }
}