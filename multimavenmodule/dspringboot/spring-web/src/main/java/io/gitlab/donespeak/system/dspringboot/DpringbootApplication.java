package io.gitlab.donespeak.system.dspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DpringbootApplication.class, args);
	}

}
